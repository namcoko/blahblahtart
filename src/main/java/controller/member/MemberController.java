package controller.member;

import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import dto.member.RegisterRequest;
import exception.AlreadyExistingMemberException;
import service.member.MemberService;

@Controller
public class MemberController {
	
	private MemberService memberService;
	
	private MemberController(MemberService memberService) {
		this.memberService = memberService;
	}

	//회원가입 get
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public String register(RegisterRequest registerRequest) {
		return "register/step1";
	}
	
	//회원가입 post
	@RequestMapping(value="/registerResult", method=RequestMethod.POST)
	public String registerResult(RegisterRequest registerRequest, Errors errors) {
		//만약에 에러가 있으면 step1으로 다시 작성하세요
		if(errors.hasErrors()) {
			return "register/step1";
		}
		try {
			memberService.registerMember(registerRequest);
			//유효성검사 통과하면(제대로 작성이 되었으면)
			return "register/step2";
			
		//이미 가입된 아이디면 step1 으로 다시 작성하세요	
		} catch(AlreadyExistingMemberException e) {
			errors.reject("email");
			return "register/step1";
		}
	}
	
	//로그인 get
	
	//로그인 post
	
	//로그아웃
	
	//탈퇴 get
	
	//탈퇴 post
	
	//email(id) 찾기 get
	
	//email(id) 찾기 post
	
	//비밀번호 찾기 get
	
	//비밀번호 찾기 - 해당 유저 post
	
	//비밀번호 찾기 - 비밀번호 변경 post
	
	//마이페이지 메인
	
	//마이페이지 수정 get
	
	//마이페이지 수정 post
	
	//장바구니
	
	//주문내역
	
	//글목록
	
}
