package service.member;

import dao.member.MemberDao;
import dto.member.MemberDto;
import dto.member.RegisterRequest;
import dto.member.UserInfo;

public class MemberServiceImple implements MemberService{
	
	private MemberDao memberDao;
	
	public MemberServiceImple(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

	@Override
	public MemberDto selectByEmail(String email) {
		return memberDao.selectByEmail(email);
	}

	@Override
	public int modifyMember(MemberDto memberDto) {
		return memberDao.modifyMember(memberDto);
	}

	@Override
	public void registerMember(RegisterRequest registerRequest) {
		 
	}

	@Override
	public void deleteMember(String email, String password) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UserInfo authenticate(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String searchByPhone(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MemberDto searchForPassword(String email, String phone, String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
