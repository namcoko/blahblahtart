package service.member;

import dto.member.MemberDto;
import dto.member.RegisterRequest;
import dto.member.UserInfo;

public interface MemberService {
	
	public abstract MemberDto selectByEmail(String email);
	public abstract int modifyMember(MemberDto memberDto); //회원정보수정
	public abstract void registerMember(RegisterRequest registerRequest); //회원가입
	public abstract void deleteMember(String email, String password); //회원탈퇴
	public abstract UserInfo authenticate(String email, String password); //로그인 시
	public abstract String searchByPhone(String phone); //email(id)찾기 시
	public abstract MemberDto searchForPassword(String email, String phone, String name); //pw찾기 시

}
