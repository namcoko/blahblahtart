package dto.member;

import java.util.Date;

public class MemberDto {
	
	private int memberNumber;
	private int memberType;		//관리자:0, 유저:1 
	private String email;		//아이디
	private String phone;		//핸드폰번호
	private String name;		//이름
	private String password;	//비밀번호
	private int gender;			//남:0, 여:1
	private String birth;		//생년월일
	private String address;		//주소
	private int point;			//포인트
	private Date joinDate;		//가입일
	
	public MemberDto() {	}
	
	public MemberDto(int memberType, String email, String phone, String name, 
					String password, int gender, String birth, String address, int point, Date joinDate) {
		this.memberType = memberType;
		this.email = email;
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.gender = gender;
		this.birth = birth;
		this.address = address;
		this.point = point;
		this.joinDate = joinDate;
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}

	public int getMemberType() {
		return memberType;
	}

	public void setMemberType(int memberType) {
		this.memberType = memberType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	
	
	

}
