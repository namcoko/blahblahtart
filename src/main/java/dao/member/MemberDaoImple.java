package dao.member;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import dto.member.MemberDto;

public class MemberDaoImple implements MemberDao {
	
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public MemberDto selectByEmail(String email) {
		return sqlSessionTemplate.selectOne("selectByEmail", email);
	}

	@Override
	public MemberDto selectByPhone(String phone) {
		return sqlSessionTemplate.selectOne("selectByPhone", phone);
	}

	@Override
	public void registerMember(MemberDto memberDto) {
		sqlSessionTemplate.insert("registerMember", memberDto);
	}

	@Override
	public void deleteMember(MemberDto memberDto) {
		sqlSessionTemplate.delete("deleteMember", memberDto.getEmail());
	}

	@Override
	public int modifyMember(MemberDto memberDto) {
		return sqlSessionTemplate.update("modifyMember", memberDto);
	}

	@Override
	public List<MemberDto> listMember() {
		return sqlSessionTemplate.selectList("listMember");
	}

	
	

}
