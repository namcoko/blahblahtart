package dao.member;

import java.util.List;

import dto.member.MemberDto;

public interface MemberDao {

	public abstract MemberDto selectByEmail(String email); //이메일(id) 찾기
	public abstract MemberDto selectByPhone(String phone); //핸드폰번호 찾기
	
	public abstract void registerMember(MemberDto memberDto); //회원 가입

	public abstract void deleteMember(MemberDto memberDto); // 회원 탈퇴 
	
	public abstract int modifyMember(MemberDto memberDto); //회원 수정
		
	public abstract List<MemberDto> listMember(); //회원리스트
		
}
